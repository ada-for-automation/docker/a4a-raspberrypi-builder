= Ada for Automation Builder: Raspberry Pi Builder
Stéphane LOS
v2020.05, 2020-05-01
:doctype: book
:lang: en
:toc: left
:toclevels: 4
:icons: font
:numbered:
:Author Initials: SLO
:source-highlighter: rouge
:imagesdir: images
:title-logo-image: image:A4A_logo1-4 TakeOff.png[Logo, pdfwidth=80%]

include::A4A_Links.asciidoc[]

== Description

=== Ada for Automation

include::A4A_Description-en.asciidoc[]

=== This project

The *Ada for Automation* framework is provided as source code.

It features demo applications which can be run on a Raspberry Pi.

Of course, before running, applications need to be built.

This project builds the Docker images used by the build process.

The basis image provides a Debian Sid base image,

* installs ssh, gcc, create user "pi" and make him sudo,

* installs git, gnat and gprbuild,

* installs libmodbus,

* installs *Ada for Automation* and builds some demo applications.

The web image uses the basis image, installs Gnoga and dependencies (Simple Components, ZanyBlue) and builds some demo applications.

The images are built using the Gitlab CI/CD feature.

At the end of the pipeline, they are published on the GitLab Registry.

They can be used of course for own applications.


